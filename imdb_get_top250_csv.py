import requests
from bs4 import BeautifulSoup as bfsp 
import pickle
import pandas as pd
from itertools import combinations

class ImdbMovies:

    path = 'https://www.imdb.com/chart/top/'
    page = requests.get(path)
    soup = bfsp(page.content,'html.parser')
    data = soup.select('tbody > tr')
    
    title_list = list()
    ranking_list = list()
    user_ratings_list = list()
    year_list = list()
    link_list = list()

    for i in data:
        title = i.select('td:nth-of-type(2) > a')[0].string.extract()
        stars = i.select('td:nth-of-type(3) > strong')[0]['title'].split(' based on ') # this gets the text of the attirubte 'title'
        ranking = float(stars[0])
        user_ratings = int(stars[1].replace(',','').split(' user ratings')[0])
        year = int(i.select('td:nth-of-type(2) > span')[0].string.extract().replace('(','').replace(')',''))
        link = 'https://www.imdb.com/title/' + i.select('td:nth-of-type(4) > div')[0]['data-titleid']
        
        title_list.append(title)
        ranking_list.append(ranking)
        user_ratings_list.append(user_ratings)
        year_list.append(year)
        link_list.append(link)   


    #with open("movies_links.pickle", "wb") as f:
    #    pickle.dump(link_list, f)

    #pd.DataFrame(zip(title_list,
    #                ranking_list,
    #                user_ratings_list, 
    #                year_list,
    #                link_list), columns=['Title','Ranking','Amount reviews', 'Year', 'Url']).to_csv(path_or_buf='imdb_movies_top250.csv',header=True,index=True,mode='w')


    #link_list = ['https://www.imdb.com/title/tt0111161','https://www.imdb.com/title/tt0068646','https://www.imdb.com/title/tt0071562']
    actors = list()
    actors_combinated = list()

    for link in link_list:
        print(link)
        page = requests.get(link)
        soup = bfsp(page.content,'html.parser')
        rows = soup.find('table',{'class':'cast_list'}).find_all('tr')[1:]
        co_actors = list()
        
        try:
            for row in rows:
                actor = row.select('td:nth-of-type(2) > a')[0].string.extract().strip('\n').strip().replace(' ','_')
                actors.append(actor)
                co_actors.append(actor)
        except IndexError:
            pass


        for i in list(combinations(co_actors,2)): # By not stablish 'list' before combinations gives an error. LIST must be here. Do not touch this
            actors_combinated.append(i)


    actors_combinated = list(dict.fromkeys(actors_combinated))
    actors = list(dict.fromkeys(actors))

    with open('actors_related.txt', 'a') as f:
        for item in actors_combinated:
            relation = item[0] + ' ' + item[1]
            f.write("%s\n" % relation)

    with open('actors.txt', 'a') as f:
        for item in actors:
            f.write("%s\n" % item)


            

