<h1>Common actors in top movies - IMDB</h1>
Analysis of the 15 most and least hired actors in the best 250 movies ranked on IMDB. It involves: BeautifulSoup, NetworkX, Matplotlib, NumPy, Pandas. In order to obtain the most and least hired actors, combinations were made between all actors avoiding repetition. Having all those combinations, NetworkX got involved to find the biggest connections between actors (a combination is a connection). This project also used nodes and edges. Finally, the result was projected in two graphs:


- Most hired actors
<img src="most-hired.png" alt="most"/>

- Least hired actors
<img src="least-hired.png" alt="least"/>
